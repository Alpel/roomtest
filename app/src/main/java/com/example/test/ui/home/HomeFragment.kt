package com.example.test.ui.home

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import com.example.test.R
import com.example.test.data.Recipe
import com.example.test.data.RecipeDatabase
import com.example.test.data.RecipeRepository
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    //private lateinit var mViewModel: com.example.test.data.ViewModel

    private lateinit var readData: List<Recipe>
    private lateinit var repository: RecipeRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        //mViewModel = ViewModelProvider(this).get(com.example.test.data.ViewModel::class.java)

        homeViewModel.viewModelScope.launch(Dispatchers.IO) {
            val dao = RecipeDatabase.getDatabase(activity!!.applicationContext).recipeDao()
            repository = RecipeRepository(dao)
            readData = repository.readAllData
        }

        val textView: TextView = root.findViewById(R.id.text_home)
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        root.testbutton.setOnClickListener {
            insertDataToDatabase()
        }

        return root
    }


    fun insertDataToDatabase() {
        val ti = recipetitle.text.toString()

        if(!TextUtils.isEmpty(ti)) {
            val recipe = Recipe(0, ti)
            repoAddRecipe(recipe)
            Toast.makeText(requireContext(), "Successfully added", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(requireContext(), "Please fill all", Toast.LENGTH_LONG).show()
        }
    }

    fun repoAddRecipe(recipe: Recipe) {
        //background thread
        homeViewModel.viewModelScope.launch(Dispatchers.IO) {
            repository.addRecipe(recipe)
        }
    }
}