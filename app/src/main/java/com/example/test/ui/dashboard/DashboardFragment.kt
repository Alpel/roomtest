package com.example.test.ui.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import com.example.test.R
import com.example.test.data.Recipe
import com.example.test.data.RecipeDatabase
import com.example.test.data.RecipeRepository
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    private lateinit var readData: List<Recipe>
    private lateinit var repository: RecipeRepository
    private var counter = 0

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)

        dashboardViewModel.viewModelScope.launch(Dispatchers.IO) {
            val dao = RecipeDatabase.getDatabase(activity!!.applicationContext).recipeDao()
            repository = RecipeRepository(dao)
            readData = repository.readAllData
        }

        counter = 0
        root.textView_id.setText(counter.toString())

        root.button_next.setOnClickListener {
            root.textView_title.setText(readData[counter].title)
            root.textView_id.setText(counter.toString())
            counter++
            if(counter == readData.size) {
                counter = 0
            }
        }

        root.button_delete.setOnClickListener {
            dashboardViewModel.viewModelScope.launch(Dispatchers.IO) {
                repository.nukeTable()
            }
        }

        val textView: TextView = root.findViewById(R.id.text_dashboard)
        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }

}