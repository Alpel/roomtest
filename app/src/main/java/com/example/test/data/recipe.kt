package com.example.test.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "recipe_data")
data class Recipe(
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    val title : String
)

/*
@Entity(tableName = "recipe_data")
data class Recipe(
    @PrimaryKey(autoGenerate = true)
    val id : String,
    val title : String,
    val subtitle : String,
    val image : String,
    val createdAt : String,
    val preparationTime : Int,
    val cookingTime : Int,
    val servings : Int,
    val instructions : List<String>,
    val ingredients : List<Ingredient>,
    val tags : List<String>,
    val comments : List<String>,
    val siteUrl : String?,
    val previewImageId : Int?
)

@Entity(tableName = "recipe_image")
data class RecipeImage (
    @PrimaryKey(autoGenerate = true)
    val id : String,
    val hasImage : Boolean,
    val isLocal : Boolean,
    val reference : String
)

@Entity(tableName = "ingredient")
data class Ingredient (
    @PrimaryKey(autoGenerate = true)
    val id : String,
    val name : String,
    val unit : String,
    val amount : Double,
    val productGroup : String?
)
*/