package com.example.test.data

class RecipeRepository(private val Dao:Dao) {

    val readAllData: List<Recipe> = Dao.readData()

    suspend fun addRecipe(recipe:Recipe) {
        Dao.addRecipe(recipe)
    }

    suspend fun nukeTable() {
        Dao.nukeTable()
    }
}