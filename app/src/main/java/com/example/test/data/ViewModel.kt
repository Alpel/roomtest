package com.example.test.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModel(application: Application): AndroidViewModel(application) {

    private val readData: List<Recipe>
    private val repository: RecipeRepository

    init {
        val dao  = RecipeDatabase.getDatabase(application).recipeDao()
        repository = RecipeRepository(dao)
        readData = repository.readAllData
    }

    fun addRecipe(recipe: Recipe) {
        //background thread
        viewModelScope.launch(Dispatchers.IO) {
            repository.addRecipe(recipe)
        }
    }
}