package com.example.test.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface Dao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addRecipe(recipe: Recipe)

    @Query("SELECT * FROM recipe_data ORDER BY id ASC")
    fun readData(): List<Recipe>

    @Query("DELETE FROM recipe_data")
    fun nukeTable()
}