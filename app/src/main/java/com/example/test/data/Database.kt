package com.example.test.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Recipe::class], version=1, exportSchema = false)
abstract class RecipeDatabase: RoomDatabase() {

    abstract fun recipeDao(): Dao

    companion object{
        @Volatile
        private var INSTANCE: com.example.test.data.RecipeDatabase? = null

        fun getDatabase(context: Context): com.example.test.data.RecipeDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null) {
                return tempInstance
            } else {
                synchronized(this) {
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        RecipeDatabase::class.java,
                        "recipe_database"
                    ).build()
                    INSTANCE=instance
                    return instance
                }
            }
        }
    }
}